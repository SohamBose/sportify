import {Dimensions, PixelRatio} from 'react-native';

const scale = Dimensions.get('window').width / 320;

exports.normalize = size => {
  const newSize = size * scale;
  return Math.round(PixelRatio.roundToNearestPixel(newSize));
};
