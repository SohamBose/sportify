const passwordValidator = require('password-validator');

exports.validateEmail = email => {
  const re = /\S+@\S+\.\S+/;
  return re.test(email);
};

exports.validatePassword = password => {
  const schema = new passwordValidator();
  schema
    .has()
    .uppercase() // Must have uppercase letters
    .has()
    .lowercase() // Must have lowercase letters
    .has()
    .digits(1) // Must have at least 2 digits
    .has()
    .symbols(1)
    .has()
    .not()
    .spaces(); // Should not have spaces
  return schema.validate(password);
};
