import Snackbar from 'react-native-snackbar';

exports.showAlert = message => {
  Snackbar.show({
    text: message,
    duration: Snackbar.LENGTH_LONG,
    backgroundColor: 'goldenrod',
    textColor: 'black',
  });
};
