import {getAllSportsUrl} from './apiConstants ';

export const getAllSportsApi = async () => {
  const data = await fetch(getAllSportsUrl);
  const jsonData = await data.json();
  return jsonData;
};
