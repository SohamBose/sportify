import {getLeagueTypesUrl} from '../api/apiConstants ';

export const getLeagueTypesApi = async () => {
  const data = await fetch(getLeagueTypesUrl);
  const jsonData = await data.json();
  return jsonData;
};
