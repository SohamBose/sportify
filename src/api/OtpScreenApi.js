import {postOtpValidate, postResendOtp} from '../api/apiConstants ';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const postPhoneOtpValidate = async (user_id, phone_otp) => {
  const TOKEN = await AsyncStorage.getItem('auth_token');
  let formData = new FormData();
  formData.append('app_user_id', user_id);
  formData.append('phone_otp', phone_otp);
  const data = await fetch(postOtpValidate, {
    method: 'POST',
    headers: new Headers({
      Authorization: 'Bearer ' + TOKEN,
      credentials: 'include',
    }),
    body: formData,
  });
  const jsonData = await data.json();
  return jsonData;
};

export const postResendOtpApi = async (user_id, phone_no) => {
  let formData = new FormData();
  formData.append('app_user_id', user_id);
  formData.append('phone_number', phone_no);
  const data = await fetch(postResendOtp, {
    method: 'POST',
    body: formData,
  });
  const jsonData = await data.json();
  return jsonData;
};
