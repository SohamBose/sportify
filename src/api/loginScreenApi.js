import {postLoginUrl} from '../api/apiConstants ';

export const postLogin = async (username, password) => {
  let formData = new FormData();
  formData.append('user_name', username);
  formData.append('password', password);
  const data = await fetch(postLoginUrl, {
    method: 'POST',
    body: formData,
  });
  const jsonData = await data.json();
  return jsonData;
};
