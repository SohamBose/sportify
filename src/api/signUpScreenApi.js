import {Platform} from 'react-native';
import {getCityUrl, postSignupUrl} from '../api/apiConstants ';

export const getCityApi = async () => {
  const data = await fetch(getCityUrl);
  const jsonData = await data.json();
  return jsonData;
};

export const postSignupUser = async signup_data => {
  let formData = new FormData();
  formData.append('name', signup_data.fullName);
  formData.append('user_name', signup_data.username);
  formData.append('email', signup_data.email);
  formData.append('password', signup_data.password);
  formData.append('conf_password', signup_data.password);
  formData.append('phone_number', signup_data.phone_num);
  formData.append('city_id', signup_data.selectedCity);
  formData.append('added_from', Platform.OS);
  formData.append('role', signup_data.userType);
  const data = await fetch(postSignupUrl, {
    method: 'POST',
    body: formData,
  });
  const jsonData = await data.json();
  return jsonData;
};
