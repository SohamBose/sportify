import {
  postUserDetailsUrl,
  updateProfileImageUrl,
  updateProfileUrl,
  updatePhoneOtpUrl,
  updatePhoneUrl,
} from '../api/apiConstants ';
import { Platform } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';


export const fetchUserData = async userID => {
  const TOKEN = await AsyncStorage.getItem('auth_token');

  let formData = new FormData();
  formData.append('user_id', userID);
  const data = await fetch(postUserDetailsUrl, {
    method: 'POST',
    headers: new Headers({
      Authorization: 'Bearer ' + TOKEN,
      credentials: 'include',
    }),
    body: formData,
  });
  const jsonData = await data.json();
  return jsonData;
};

export const updateProfileImage = async (userID, image_uri, img_type, image_name,) => {
  const TOKEN = await AsyncStorage.getItem('auth_token');

  let formData = new FormData();
  formData.append('user_id', userID);
  formData.append('Files', {
    uri:
      Platform.OS === 'android' ? image_uri : image_uri.replace('file:///', ''),
    type: img_type,
    name: image_name,
  });
  const xhr = new XMLHttpRequest();

  return new Promise((resolve, reject) => {
    xhr.open('POST', updateProfileImageUrl);
    xhr.setRequestHeader('Authorization', 'Bearer ' + TOKEN);
    xhr.send(formData);
    xhr.onreadystatechange = e => {
      if (xhr.readyState !== 4) {
        return;
      }
      if (xhr.status === 200) {
        console.log('success', xhr.responseText);
        resolve(JSON.parse(xhr.responseText));
      } else {
        console.log('EditImageerror', xhr.responseText);
      }
    };
  });
};

export const updateProfile = async userData => {
  const TOKEN = await AsyncStorage.getItem('auth_token');

  let formData = new FormData();
  formData.append('user_id', userData.userID);
  formData.append('name', userData.fullname);
  formData.append('user_name', userData.username);
  formData.append('email', userData.Email);
  formData.append('age', userData.Age);
  const data = await fetch(updateProfileUrl, {
    method: 'POST',
    headers: new Headers({
      Authorization: 'Bearer ' + TOKEN,
      credentials: 'include',
    }),
    body: formData,
  });
  const jsonData = await data.json();
  return jsonData;
};

export const getPhoneOTP = async (userID, phone_no) => {
  const TOKEN = await AsyncStorage.getItem('auth_token');

  let formData = new FormData();
  formData.append('user_id', userID);
  formData.append('phone_number', phone_no);

  const data = await fetch(updatePhoneOtpUrl, {
    method: 'POST',
    headers: new Headers({
      Authorization: 'Bearer ' + TOKEN,
      credentials: 'include',
    }),
    body: formData,
  });
  const jsonData = await data.json();
  return jsonData;
};

export const updatePhoneNumber = async (userID, phone_no, OTP) => {
  const TOKEN = await AsyncStorage.getItem('auth_token');

  let formData = new FormData();
  formData.append('user_id', userID);
  formData.append('phone_number', phone_no);
  formData.append('otp', OTP);

  const data = await fetch(updatePhoneUrl, {
    method: 'POST',
    headers: new Headers({
      Authorization: 'Bearer ' + TOKEN,
      credentials: 'include',
    }),
    body: formData,
  });
  const jsonData = await data.json();
  return jsonData;
};

