export const apiProtocol = 'http://';
//export const rootApiUrl = '35.83.58.228:5000/api/';
export const rootApiUrl = '35.83.58.228:5005/api/';
// export const rootApiUrl = '35.83.58.228:5000/api/'; 
export const rootApiVersion = 'v1/';
//export const rootUrl = '35.83.58.228:5000/';
export const rootUrl = '35.83.58.228:5005/';

//Example: http://35.83.58.228:5000/img/user/4266486_3.jpg

export const imageUrl = `${apiProtocol}${rootUrl}img/user/`; //For fetching Image
export const getCityUrl = `${apiProtocol}${rootApiUrl}${rootApiVersion}user/get_all_cities_list`;
export const postLoginUrl = `${apiProtocol}${rootApiUrl}${rootApiVersion}user/app_user_login`;
export const postSignupUrl = `${apiProtocol}${rootApiUrl}${rootApiVersion}user/app_user_sign_up`;
export const getAllSportsUrl = `${apiProtocol}${rootApiUrl}${rootApiVersion}sport/get_all_sports_list`;
export const getLeagueTypesUrl = `${apiProtocol}${rootApiUrl}${rootApiVersion}league/get_all_leagues_list`;


export const postUserDetailsUrl = `${apiProtocol}${rootApiUrl}${rootApiVersion}user/get_app_user_details_by_id`;
export const updateProfileImageUrl = `${apiProtocol}${rootApiUrl}${rootApiVersion}user/app_user_update_profile_image`;
export const updateProfileUrl = `${apiProtocol}${rootApiUrl}${rootApiVersion}user/app_user_update_profile`;
export const updatePhoneOtpUrl = `${apiProtocol}${rootApiUrl}${rootApiVersion}user/app_user_change_phone_number`;
export const updatePhoneUrl = `${apiProtocol}${rootApiUrl}${rootApiVersion}user/app_user_update_phone_number`;

export const postOtpValidate = `${apiProtocol}${rootApiUrl}${rootApiVersion}user/app_user_phone_number_verify`;
export const postResendOtp = `${apiProtocol}${rootApiUrl}${rootApiVersion}user/app_user_resend_otp`;
