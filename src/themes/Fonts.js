const Fonts = {
    Roboto_Regular: 'Roboto-Regular',
    Roboto_Bold: 'Roboto-Bold',
    Roboto_Medium: 'Roboto-Medium',
    Poppins_SemiBold: 'Poppins-SemiBold',
    Poppins_Bold: 'Poppins-Bold',
    MontserratRegular: 'Montserrat-Regular',
    MontserratLight: 'Montserrat-Light',
    MontserratThin: 'Montserrat-Thin',
    MontserratBold: 'Montserrat-Bold',
}
export default Fonts;