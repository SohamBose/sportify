import React, {useRef, useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ImageBackground,
  ScrollView,
  Modal,
} from 'react-native';

import {Image} from 'react-native-elements';

import Statusbar from '../components/StatusBar';
import {useNavigation, useScrollToTop} from '@react-navigation/native';
import Colors from '../themes/Colors';
const backgroundImage = require('../assets/splash_bg.png');
const logoImage = require('../assets/logo_icon.png');
import {normalize} from '../utils/Dimention';
import {LoginAction} from '../store/login-slice';

const SplashScreen = () => {
  const dispatch = useDispatch();
  const [modalVisible, setModalVisible] = useState(true);
  const navigation = useNavigation();
  const scrollViewRef = useRef();
  useScrollToTop(scrollViewRef);

  useEffect(() => {
    scrollViewRef.current.scrollToEnd({animated: true});
    const timer = setTimeout(() => {
      navigateTo();
    }, 3000);

    return () => clearTimeout(timer);
  }, []);

  const navigateTo = () => {
    setModalVisible(false);
    dispatch(LoginAction.splashScreenRemove());
    navigation.navigate('Login');
  };

  return (
    <SafeAreaView style={styles.screenContainer}>
      <Statusbar backgroundColor={'black'} />
      <ScrollView style={{flexGrow: 1}} ref={scrollViewRef}>
        <ImageBackground
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          source={backgroundImage}>
          <Modal animationType="none" transparent={true} visible={modalVisible}>
            <View style={styles.imageContent}>
              <Image source={logoImage} style={styles.logoStyle} />
            </View>

            <View style={styles.textStyle}>
              <Text
                style={{
                  fontSize: normalize(10),
                  color: Colors.sports,
                  fontWeight: 'normal',
                }}>
                Copyright © Sportifi 2021
              </Text>
            </View>
          </Modal>

          <View style={styles.subContainer}></View>
        </ImageBackground>
      </ScrollView>
    </SafeAreaView>
  );
};
export default SplashScreen;

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
  },
  logoStyle: {
    width: normalize(200),
    height: normalize(200),
    resizeMode: 'contain',
  },
  imageContent: {
    justifyContent: 'center',
    alignItems: 'center',
    top: '30%',
  },
  textStyle: {
    width: '100%',
    height: normalize(50),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  subContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    height: '100%',
    paddingBottom: '200%',
  },
});
