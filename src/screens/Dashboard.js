import React, {Fragment, useState, useEffect, useRef} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  View,
  StyleSheet,
  ImageBackground,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Colors from '../themes/Colors';
import {normalize} from '../utils/Dimention';
import {Button, Text} from 'react-native-elements';
import {Picker} from '@react-native-picker/picker';

import Statusbar from '../components/StatusBar';
import CustomHeader from '../components/CustomHeader';
import {useNavigation} from '@react-navigation/native';
import ProgressLoader from '../components/ProgressLoader';
import {getCity} from '../store/singup-slice';

const backgroundImage = require('../assets/league_type_bg_image.png');

const Dashboard = () => {
  const navigation = useNavigation();
  const loader = useSelector(state => state.loader.loading);
  const dispatch = useDispatch();
  const cities = useSelector(state => state.signup.city);
  const [selectedCity, setSelectedCity] = useState(0);

  useEffect(() => {
    dispatch(getCity());
  }, []);
  return (
    <>
      <Statusbar backgroundColor={'black'} />
      <ProgressLoader loading={loader} />
      <CustomHeader navigationProps={navigation} />
      <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
        <View style={styles.countryContainer}>
          <View
            style={{
              borderWidth: 1,
              borderColor: Colors.sports,
              borderRadius: 4,
              backgroundColor: Colors.sports,
              height: normalize(25),
              justifyContent: "center"
            }}>
            <Picker
              selectedValue={selectedCity}
              style={{
                width: normalize(110),
                color: 'black',
              }}
              dropdownIconColor="black"
              onValueChange={(itemValue, itemIndex) =>
                setSelectedCity(itemValue)
              }>
              <Picker.Item key={0} label={'Country'} value={0} />
              {cities.map(item => (
                <Picker.Item key={item.id} label={item.name} value={item.id} />
              ))}
            </Picker>
          </View>
        </View>
        <View style={styles.subContainer}></View>
      </ImageBackground>
    </>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  subContainer: {
    flex: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: '80%',
    marginTop: Platform.OS == 'android' ? normalize(-10) : '50%',
    paddingBottom: Platform.OS == 'android' ? normalize(40) : '50%',
  },
  countryContainer: {
    flex: 1,
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
});

export default Dashboard;
