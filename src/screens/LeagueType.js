import React, { useState, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  View,
  StyleSheet,
  ImageBackground,
  Image,
  ScrollView,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import Colors from '../themes/Colors';
import { normalize } from '../utils/Dimention';
import { Button, Text } from 'react-native-elements';
import { showAlert } from '../utils/Toast';

import Statusbar from '../components/StatusBar';

const backgroundImage = require('../assets/league_type_bg_image.png');
const logoImage = require('../assets/logo_icon.png');
const ltype_icon1 = require('../assets/league_type1.png');
const ltype_icon2 = require('../assets/league_type2.png');
import ProgressLoader from '../components/ProgressLoader';
import {
  getLeagueTypes,
  updateLeagueTypesList,
} from '../store/leagueTypes-slice';

const LeagueType = () => {
  const loader = useSelector(state => state.loader.loading);
  const isLoggedin = useSelector(state => state.login.isLoggedin);

  const dispatch = useDispatch();
  const types = useSelector(state => state.leagueTypesSlice.types);
  const navigation = useNavigation();
  const scrollViewRef = useRef();


  useEffect(() => {
    scrollViewRef.current.scrollToEnd({animated: true});
  });

  useFocusEffect(
    React.useCallback(() => {
      //Screen focused
      scrollViewRef.current.scrollToEnd({ animated: true });
      dispatch(getLeagueTypes());

      if (!isLoggedin) {
        const backAction = () => {
          BackHandler.exitApp();
          return true;
        };

        const backHandler = BackHandler.addEventListener(
          'hardwareBackPress',
          backAction,
        );

        return () => backHandler.remove();
      }

    }, [])
  );

  

  const onGridItemClick = item => {
    try {
      const objIndex = types.findIndex(obj => obj.id == item.id);
      dispatch(updateLeagueTypesList(objIndex));
    } catch (e) {

    }
  };

  const nextHandler = () => {
    const selected = types.filter(item => {
      return item.selected_stat === true;
    });
    if (selected.length === 0) {
      return showAlert('Please select atleast 1 league');
    }
    if (!isLoggedin) {
      navigation.navigate('SportInterest');
    } else {
      navigation.navigate('Dashboard');
    }
  };

  const skipHandler = () => {
    try {
      if (!isLoggedin) {
        navigation.navigate('SportInterest');
      } else {
        navigation.navigate('Dashboard');
      }
    } catch (error) {
      console.log("FromLeague--> ", error);
    }


  };

  return (
    <View style={styles.screenContainer}>
      <Statusbar backgroundColor={'black'} />
      <ProgressLoader loading={loader} />
      <ScrollView ref={scrollViewRef}>
        <ImageBackground
          source={backgroundImage}
          style={styles.backgroundImage}>
          <Image source={logoImage} style={styles.logoImage} />
          <View style={styles.subContainer}>
            <Text
              style={{
                fontSize: normalize(18),
                color: '#fff',
                fontWeight: 'normal',
              }}>
              League Type
            </Text>
            {types.map(item => (
              <TouchableOpacity
                key={item.id}
                style={[styles.ltype, { marginTop: '10%' }]}
                onPress={onGridItemClick.bind(this, item)}>
                <View
                  style={[
                    styles.league_container,
                    item.selected_stat
                      ? { backgroundColor: Colors.sports }
                      : { backgroundColor: '#fff' },
                  ]}>
                  {item.id % 2 === 0 && (
                    <Image source={ltype_icon1} style={styles.LImage} />
                  )}
                  {item.id % 2 !== 0 && (
                    <Image source={ltype_icon2} style={styles.LImage} />
                  )}
                  <Text
                    style={[styles.ltypeBtnTitle, { marginLeft: normalize(30) }]}>
                    {item.name}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
            <View style={styles.buttonConatiner}>
              <Button
                title="SKIP"
                titleStyle={{
                  color: 'black',
                  fontSize: normalize(15),
                }}
                buttonStyle={{
                  backgroundColor: Colors.sports,
                  width: normalize(100),
                }}
                onPress={() => skipHandler()}
              />
              <Button
                title="NEXT"
                titleStyle={{
                  color: 'black',
                  fontSize: normalize(15),
                }}
                buttonStyle={{
                  backgroundColor: Colors.sports,
                  width: normalize(100),
                }}
                onPress={nextHandler}
              />
            </View>
          </View>
        </ImageBackground>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoImage: {
    flex: 1,
    resizeMode: 'center',
    justifyContent: "flex-start"
  },
  subContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    marginTop: normalize(-60),
  },
  buttonConatiner: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
    paddingVertical: normalize(20),
  },
  ltype: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  ltypeBtnTitle: {
    color: 'black',
    fontSize: normalize(13),
    textAlign: 'center',
    fontWeight: '500',
  },
  league_container: {
    width: '80%',
    justifyContent: 'flex-start',
    paddingVertical: normalize(4),
    paddingHorizontal: normalize(3),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  LImage: {
    height: normalize(30),
    width: normalize(30),
    alignItems: 'center',
    alignSelf: 'center',
  },
});

export default LeagueType;
