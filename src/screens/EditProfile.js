import React, { Fragment, useRef, useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  ImageBackground,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
  Modal,
  Platform,
  PermissionsAndroid,
} from 'react-native';

import { showAlert } from '../utils/Toast';
import Colors from '../themes/Colors';
import { normalize } from '../utils/Dimention';
import { imageUrl } from '../api/apiConstants ';
import { Input, Button, Text, Avatar, Image, Icon } from 'react-native-elements';
import Statusbar from '../components/StatusBar';
import CustomHeader from '../components/CustomHeader';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import { validateEmail } from '../utils/emailValidator';
import Card from '../components/Card';
import {
  getExistingData,
  updateImage,
  updateProfileDetails,
  getOTP,
} from '../store/editProfile-slice';
import ProgressLoader from '../components/ProgressLoader';
import { Alert } from 'react-native';
import * as ImagePicker from 'react-native-image-picker';
const backgroundImage = require('../assets/edit_prof_bg.png');
const dummy_prof = require('../assets/edit_prof_dummy_img.jpg');
import AsyncStorage from '@react-native-async-storage/async-storage';

//global id
let id = '', existing_data = "";


const imagePickerOptions = [
  {
    title: 'Camera',
    type: 'Camera',
    iconName: "camera",
    iconType: "material-icons",
    options: {
      saveToPhotos: true,
      mediaType: 'photo',
      includeBase64: false,
      quality: 1,
    },
  },
  {
    title: 'Gallery',
    type: 'Library',
    iconName: "photo",
    iconType: "material-icons",
    options: {
      maxHeight: 200,
      maxWidth: 200,
      selectionLimit: 1,
      mediaType: 'photo',
      includeBase64: false,
      quality: 1,
    },
  },
];

const EditProfile = () => {
  const loader = useSelector(state => state.loader.loading);
  const dispatch = useDispatch();
  existing_data = useSelector(state => state.editprofile.userdata);
  const navigation = useNavigation();
  const scrollViewRef = useRef();
  const phoneInput = React.createRef();

  const [fullName, setFullName] = useState('');
  const [phone, setPhone] = useState(null);
  const [email, setEmail] = useState('');
  const [age, setAge] = useState('');
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [imageResponse, setImageResponse] = useState(null);
  const [removeImage, setRemoveImage] = useState(null);
  const [editType, setEditType] = useState("");

  useEffect(async () => {
    id = await AsyncStorage.getItem('global_ID');
    console.log("ID---> ",id);
    dispatch(getExistingData(id));
    scrollViewRef.current.scrollToEnd({ animated: true });
    setImageResponse(imageResponse);
    setRemoveImage(removeImage);
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      //Screen focused
      async function getID() {
        try {
          id = await AsyncStorage.getItem('global_ID');
          dispatch(getExistingData(id));
          scrollViewRef.current.scrollToEnd({ animated: true });
          setImageResponse(imageResponse);
          setRemoveImage(removeImage);
        } catch (e) {

        }
      }
      getID()

    }, [])
  );


  const uploadPhoto = () => {
    if (imageResponse != null && !isCancelled()) {
      dispatch(
        updateImage(
          id,
          imageResponse.assets[0].uri,
          imageResponse.assets[0].type,
          imageResponse.assets[0].fileName,
        ),
      );
    } else {
      //showAlert("Please select a photo")
    }
  };

  const changePhone = async () => {

    const phone_num = '' + existing_data.phone;
    if (phone != null) {
      if (phone != phone_num) {
        if (phone.length == 10) {
          dispatch(getOTP(id, phone, navigation));
        } else {
          showAlert("Please provide a valid contact number");
        }
      }
    }
  };

  const submitData = () => {

    //uploadPhoto();
    if (fullName || email || age) {
      const userDetails = {
        userID: id,
        fullname: !fullName ? existing_data.name : fullName,
        username: existing_data.user_name,
        Email: !email ? existing_data.email : email,
        Age: !age ? existing_data.age : age,
      };
      console.log('EditData===> ', userDetails);
      dispatch(updateProfileDetails(userDetails, navigation));
    }

    //For Photo upload
    uploadPhoto();


  };

  const isCancelled = () => {
    try {
      if (imageResponse.didCancel != undefined) {
        //User cancelled
        return true;
      } else {
        //User didnot cancel
        return false;
      }
    } catch (error) {
      //console.log(error);
      return false;
    }
  };

  const onImageClick = React.useCallback(async (type, options) => {
    if (type === 'Camera') {
      if (Platform.OS == 'android') {
        if (Platform.Version >= 23) {
          //For Marshmellow and Above
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            // Permission Granted
            setRemoveImage(false);
            ImagePicker.launchCamera(options, setImageResponse);
            setIsModalVisible(false);
            // uploadPhoto();
          } else {
            Alert.alert(
              'Permission',
              'In order to run the app properly, Sportifi needs permission to access your Camera',
              [
                {
                  text: 'Deny',
                  style: 'cancel',
                },
                { text: 'Grant' },
              ],
            );
            // Permission Denied
          }
        } else {
          setRemoveImage(false);
          ImagePicker.launchCamera(options, setImageResponse);
          setIsModalVisible(false);
        }
      } else {
        setRemoveImage(false);
        ImagePicker.launchCamera(options, setImageResponse);
        setIsModalVisible(false);
      }
    } else {
      setRemoveImage(false);
      ImagePicker.launchImageLibrary(options, setImageResponse);
      setIsModalVisible(false);
    }
  }, []);

  const OpenPicker = () => (
    <Modal animationType="fade" transparent={true} visible={isModalVisible}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>

          <Text style={{ top: normalize(6), fontSize: normalize(12), }}>Select an option</Text>
          <View style={{
            backgroundColor: '#A2A2A2',
            height: normalize(1),
            width: "100%"
          }}></View>

          {imagePickerOptions.map(({ title, type, options, iconName, iconType }) => {
            return (
              <TouchableOpacity
                key={title}
                onPress={() => onImageClick(type, options)}
                style={styles.pickerOptionStyle}>
                <Icon
                  name={iconName}
                  type={iconType}
                  size={normalize(20)}
                  color={Colors.greenColor}
                  containerStyle={styles.pickerIconStyle}
                />
                <Text
                  key={title}
                  style={styles.pickerTextStyle}>
                  {title}
                </Text>
              </TouchableOpacity>
            );
          })}

          <TouchableOpacity
            onPress={() => (setRemoveImage(true), setIsModalVisible(false))}
            style={styles.pickerOptionStyle}>
            <Icon
              name="delete"
              type="material-icons"
              size={normalize(20)}
              color={Colors.greenColor}
              containerStyle={styles.pickerIconStyle}
            />
            <Text style={styles.pickerTextStyle}>
              Remove
            </Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => setIsModalVisible(false)}>
            <Text style={{ fontSize: normalize(12) }}>Close</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );

  return (
    <View style={styles.screenContainer}>
      <ProgressLoader loading={loader} />
      <Statusbar backgroundColor={'black'} />
      <CustomHeader navigationProps={navigation} />
      <ScrollView style={{ flexGrow: 1 }} ref={scrollViewRef}>
        <ImageBackground
          source={backgroundImage}
          style={styles.backgroundImage}>
          <Card
            style={
              ([styles.backgroundImage],
              {
                backgroundColor: 'rgba(52, 52, 52, 0.8)',
                top: normalize(25),
                width: '90%',
                marginBottom: normalize(50),
                borderRadius: normalize(5),
              })
            }>
            <Text
              style={{
                fontSize: normalize(16),
                color: '#fff',
                fontWeight: 'normal',
                top: normalize(20),
              }}>
              Edit Profile
            </Text>

            <TouchableOpacity
              onPress={() => setIsModalVisible(true)}
              style={{
                flex: 1,
                paddingVertical: normalize(45),
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'center',
              }}>
              <Image
                source={{
                  uri: removeImage
                    ? null
                    : imageResponse == null
                      ? imageUrl + existing_data.image
                      : !isCancelled()
                        ? imageResponse.assets[0].uri
                        : removeImage
                          ? null
                          : imageUrl + existing_data.image,
                }}
                PlaceholderContent={
                  existing_data.image ? <ActivityIndicator color="blue" size="large" />
                    : null
                }
                style={styles.profileImg}
              />

              <Icon
                name="pluscircle"
                type="ant-design"
                size={normalize(22)}
                color={Colors.greenColor}
                containerStyle={styles.addImageStyle}
              />
            </TouchableOpacity>

            <OpenPicker />

            <View style={styles.subContainer}>
              <Input
                defaultValue={existing_data.name}
                placeholder="Full Name"
                color={Colors.sports}
                placeholderTextColor={Colors.sports}
                onChangeText={inputtext => setFullName(inputtext)}
                inputContainerStyle={styles.inputContainer}
                inputStyle={styles.inputstyleContainer}
                leftIcon={
                  <Icon
                    name="user-o"
                    type="font-awesome"
                    size={normalize(18)}
                    color={Colors.sports}
                  />
                }
              />

              {/* <Input
                ref={phoneInput}
                defaultValue={!existing_data.phone ? "" : '' + existing_data.phone}
                placeholderTextColor={Colors.sports}
                onChangeText={inputtext => setPhone(inputtext)}
                color={Colors.sports}
                keyboardType="number-pad"
                inputContainerStyle={styles.inputContainer}
                inputStyle={styles.inputstyleContainer}
                leftIcon={
                  <Icon
                    name="call"
                    type="zocial"
                    size={normalize(18)}
                    color={Colors.sports}
                  />
                }
                rightIcon={
                  <Icon
                    name="keyboard-arrow-right"
                    type="material-icons"
                    size={normalize(22)}
                    color={Colors.whiteColor}
                    iconStyle={{ right: normalize(2) }}
                    onPress={() => changePhone()}
                  />
                }
              /> */}

              <Input
                defaultValue={existing_data.email}
                placeholder="Email"
                placeholderTextColor={Colors.sports}
                onChangeText={inputtext => setEmail(inputtext)}
                color={Colors.sports}
                inputContainerStyle={styles.inputContainer}
                inputStyle={styles.inputstyleContainer}
                leftIcon={
                  <Icon
                    name="envelope-o"
                    type="font-awesome"
                    size={normalize(18)}
                    color={Colors.sports}
                  />
                }
              />

              <Input
                placeholder={!existing_data.age ? "Age" : '' + existing_data.age}
                placeholderTextColor={Colors.sports}
                onChangeText={inputtext => setAge(inputtext)}
                color={Colors.sports}
                keyboardType="number-pad"
                inputContainerStyle={styles.inputContainer}
                inputStyle={styles.inputstyleContainer}
                leftIcon={
                  <Icon
                    name="calendar"
                    type="font-awesome-5"
                    size={normalize(18)}
                    color={Colors.sports}
                  />
                }
              />

              <Button
                title="SAVE"
                titleStyle={{
                  color: 'black',
                  fontSize: normalize(15),
                }}
                buttonStyle={{
                  backgroundColor: Colors.sports,
                  width: normalize(215),
                }}
                containerStyle={{ top: normalize(25) }}
                onPress={() => {
                  submitData();
                }}
              />
            </View>
          </Card>
        </ImageBackground>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  subContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    width: '80%',
    paddingBottom: normalize(80),
    top: normalize(5),
  },

  inputContainer: {
    borderBottomWidth: 0,
    height: normalize(35),
    borderRadius: 2,
    backgroundColor: 'black',
    color: 'white',
    width: '100%',
    paddingLeft: 20,
    marginTop: 10,
    marginBottom: 10,
  },
  inputstyleContainer: {
    fontSize: normalize(15),
  },
  profileImg: {
    width: normalize(65),
    height: normalize(65),
    borderRadius: normalize(200 / 2),
    borderColor: Colors.sports,
    borderWidth: 1,
  },
  addImageStyle: {
    position: 'absolute',
    alignSelf: 'flex-end',
    right: normalize(103),
    bottom: '68%',
  },

  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040',

  },
  activityIndicatorWrapper: {
    backgroundColor: '#fff',
    height: normalize(200),
    width: normalize(180),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'column',
    borderRadius: normalize(2),
    marginTop: normalize(8),
  },
  pickerOptionStyle: {
    flexDirection: "row",
    display: "flex",
    alignItems: 'center',
    justifyContent: 'space-around',
    right: normalize(30),
    paddingTop: normalize(10)
  },
  pickerTextStyle: {
    fontWeight: '700',
    fontSize: normalize(12),
    left: normalize(10),
  }

});

export default EditProfile;
