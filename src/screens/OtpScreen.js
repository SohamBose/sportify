import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  ImageBackground,
  Image,
  TouchableOpacity,
} from 'react-native';
import CountDown from 'react-native-countdown-component';
import { Input, Text, Button, Icon, CheckBox } from 'react-native-elements';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Colors from '../themes/Colors';
import { normalize } from '../utils/Dimention';
import Statusbar from '../components/StatusBar';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { showAlert } from '../utils/Toast';
import { phoneOtpValidate, resendOtp } from '../store/singup-slice';
import { updatePhone } from '../store/editProfile-slice';

const backgroundImage = require('../assets/signup_bg.png');
const logoImage = require('../assets/logo_icon.png');

const OtpScreen = props => {
  const [code, setCode] = useState('');
  const [timerStatus, setTimerStatus] = useState(true);
  const [timerId, setTimerId] = useState('1');
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const otpValidateHandler = () => {
    if (code === '') {
      return showAlert('Otp cannot be empty');
    }
    if (code != '0000') {
      return showAlert('Otp not valid please enter 0000(development mode)');
    }
    if (props.route.params.isEdit) {
      dispatch(updatePhone(props.route.params.user_id, props.route.params.phone_no, code, navigation));
    } else {
      dispatch(phoneOtpValidate(props.route.params.user_id, code, navigation));
    }

  };

  const resendOtpvalidator = () => {
    if (timerStatus === true) {
      return showAlert('Wait for the timer to finish');
    }
    setTimerStatus(true);
    setTimerId(Math.random().toString());
    dispatch(resendOtp(props.route.params.user_id, props.route.params.phone_no))
  };

  return (
    <View style={styles.screenContainer}>
      <Statusbar backgroundColor={'black'} />
      <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
        <Image source={logoImage} style={styles.logoImage} />
        <View style={styles.subContainer}>
          <View style={styles.textConatiner}>
            <Text
              style={{
                fontSize: normalize(12),
                color: Colors.sports,
                textAlign: 'center',
              }}>
              Please enter the OTP sent to your Registered mobile No
            </Text>
          </View>
          <OTPInputView
            style={{ width: normalize(200), height: normalize(50) }}
            pinCount={4}
            code={code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
            onCodeChanged={input => {
              setCode(input);
            }}
            autoFocusOnLoad
            codeInputFieldStyle={styles.underlineStyleBase}
            codeInputHighlightStyle={styles.underlineStyleHighLighted}
          // onCodeFilled={code => {
          //   console.log(`Code is ${code}, you are good to go!`);
          // }}
          // onCodeFilled={otp => setCode(otp)}
          />
          <View style={styles.resendConatiner}>
            <TouchableOpacity onPress={resendOtpvalidator}>
              <Text
                style={{
                  fontSize: normalize(12),
                  color: 'white',
                }}>
                Resend OTP
              </Text>
            </TouchableOpacity>
            <View>
              <CountDown
                id={timerId}
                running={timerStatus}
                until={60}
                size={normalize(10)}
                onFinish={() => setTimerStatus(false)}
                digitStyle={{ backgroundColor: 'grey' }}
                digitTxtStyle={{ color: 'white' }}
                timeToShow={['M', 'S']}
                timeLabels={{ m: 'MM', s: 'SS' }}
              />
            </View>
          </View>
          <Button
            title="CONFIRM"
            titleStyle={{
              color: 'black',
              fontSize: normalize(15),
            }}
            buttonStyle={{
              backgroundColor: Colors.sports,
              width: normalize(260),
              marginTop: normalize(50),
            }}
            onPress={otpValidateHandler}
          />
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoImage: {
    flex: 1,
    resizeMode: 'center',
  },
  subContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '80%',
    marginBottom: normalize(10),
  },
  textConatiner: {
    marginVertical: normalize(10),
    width: '90%',
    alignItems: 'center',
  },
  borderStyleBase: {
    width: 30,
    height: 45,
  },
  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
  },
  underlineStyleHighLighted: {
    borderColor: Colors.sports,
  },
  resendConatiner: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: normalize(10),
    width: '80%',
  },
});

export default OtpScreen;
