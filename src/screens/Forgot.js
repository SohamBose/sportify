import React, {useEffect, useRef} from 'react';
import {
  View,
  StyleSheet,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {Input, Text, Button, Icon, CheckBox} from 'react-native-elements';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Colors from '../themes/Colors';
import {normalize} from '../utils/Dimention';
import Statusbar from '../components/StatusBar';
import {useNavigation} from '@react-navigation/native';

const backgroundImage = require('../assets/signup_bg.png');
const logoImage = require('../assets/logo_icon.png');

const OtpScreen = () => {
  const navigation = useNavigation();

  const scrollViewRef = useRef();

  useEffect(() => {
    scrollViewRef.current.scrollToEnd({animated: true});
  });

  return (
    <View style={styles.screenContainer}>
      <Statusbar backgroundColor={'black'} />
      <ScrollView style={{flexGrow: 1}} ref={scrollViewRef}>
        <ImageBackground
          source={backgroundImage}
          style={styles.backgroundImage}>
          <Image source={logoImage} style={styles.logoImage} />
          <View style={styles.subContainer}>
            <View style={styles.textConatiner}>
              <Text
                style={{
                  fontSize: normalize(12),
                  color: Colors.sports,
                  textAlign: 'center',
                }}>
                Please enter the OTP sent to your Registered mobile No
              </Text>
            </View>
            <OTPInputView
              style={{width: normalize(200), height: normalize(50)}}
              pinCount={4}
              // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
              // onCodeChanged = {code => { this.setState({code})}}
              autoFocusOnLoad
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              onCodeFilled={code => {
                console.log(`Code is ${code}, you are good to go!`);
              }}
            />
            <View style={styles.resendConatiner}>
              <TouchableOpacity onPress={() => console.warn('pressed')}>
                <Text
                  style={{
                    fontSize: normalize(12),
                    color: 'white',
                  }}>
                  Resend OTP
                </Text>
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: normalize(12),
                  color: 'white',
                }}>
                00.59
              </Text>
            </View>
            <Button
              title="OTP VALIDATE"
              titleStyle={{
                color: 'black',
                fontSize: normalize(15),
              }}
              buttonStyle={{
                backgroundColor: Colors.sports,
                width: normalize(260),
                marginVertical: normalize(30),
              }}
              onPress={() => navigation.navigate('LeagueType')}
            />
            <Input
              editable={false}
              placeholder="New Password"
              placeholderTextColor={Colors.sports}
              color={Colors.sports}
              secureTextEntry={true}
              inputContainerStyle={{borderColor: Colors.sports}}
              leftIcon={
                <Icon name="lock" size={normalize(24)} color={Colors.sports} />
              }
            />
            <Button
              disabled={true}
              title="CONFIRM"
              titleStyle={{
                color: 'black',
                fontSize: normalize(15),
              }}
              buttonStyle={{
                backgroundColor: Colors.sports,
                width: normalize(260),
                //   marginTop: normalize(50),
              }}
              onPress={() => navigation.navigate('LeagueType')}
            />
          </View>
        </ImageBackground>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoImage: {
    flex: 1,
    resizeMode: 'center',
    marginTop: normalize(-50),
  },
  subContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '80%',
    marginBottom: normalize(10),
    marginTop: normalize(-40),
    paddingBottom: normalize(40),
  },
  textConatiner: {
    marginVertical: normalize(10),
    width: '90%',
    alignItems: 'center',
  },
  borderStyleBase: {
    width: 30,
    height: 45,
  },
  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
  },
  underlineStyleHighLighted: {
    borderColor: Colors.sports,
  },
  resendConatiner: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: normalize(10),
    width: '80%',
  },
});

export default OtpScreen;
