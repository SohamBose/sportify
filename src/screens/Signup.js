import React, { useState, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  View,
  StyleSheet,
  ImageBackground,
  Image,
  ScrollView,
  Platform
} from 'react-native';
import { Picker } from '@react-native-picker/picker';
import Colors from '../themes/Colors';
import { normalize } from '../utils/Dimention';
import { Input, Text, Button, Icon, CheckBox } from 'react-native-elements';
import Statusbar from '../components/StatusBar';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import ProgressLoader from '../components/ProgressLoader';
import { showAlert } from '../utils/Toast';
import { validateEmail, validatePassword } from '../utils/emailValidator';


const backgroundImage = require('../assets/signup_bg.png');
const logoImage = require('../assets/logo_icon.png');
import { getCity, signupUser } from '../store/singup-slice';

const Signup = () => {
  const loader = useSelector(state => state.loader.loading);
  const dispatch = useDispatch();
  const cities = useSelector(state => state.signup.city);
  const scrollViewRef = useRef();
  const navigation = useNavigation();

  const [fullName, setFullName] = useState('');
  const [username, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [selectedCity, setSelectedCity] = useState(0);
  const [userType, setUserType] = useState(0);
  const [isPasswordVisible, setIsPasswordVisible] = useState(true);
  const [countryCode, setCountryCode] = useState('0');

  useFocusEffect(
    React.useCallback(() => {
      //Screen focused
      fullNameInputHandeler("");
      usernameInputHandeler("");
      emailInputHandeler("");
      mobileNoInputhandler("");
      passwordInputHandeler("");
      setUserType(0);
      setCountryCode('0');
      setSelectedCity(0);

    }, [])
  );


  useEffect(() => {
    setUserType(userType);
    scrollViewRef.current.scrollToEnd({ animated: true });
    setIsPasswordVisible(isPasswordVisible);

    dispatch(getCity());
  }, []);

  const getPasswordVisiblity = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };

  const fullNameInputHandeler = inputText => {
    setFullName(inputText);
  };
  const usernameInputHandeler = inputText => {
    setUserName(inputText);
  };
  const emailInputHandeler = inputText => {
    setEmail(inputText);
  };
  const passwordInputHandeler = inputText => {
    setPassword(inputText);
  };
  const mobileNoInputhandler = inputText => {
    setMobileNo(inputText);
  };

  const signupHandler = () => {
    if (fullName.length === 0) {
      return showAlert('Fullname cannot be empty');
    }
    if (username.length === 0) {
      return showAlert('Username cannot be empty');
    }
    if (email.length === 0) {
      return showAlert('Email cannot be empty');
    }
    if (!validateEmail(email)) {
      return showAlert('Enter valid email');
    }
    if (password.length < 8) {
      return showAlert('Password cannot be less than 8 character');
    }
    if (!validatePassword(password)) {
      return showAlert(
        'Password must has 1 uppercase, 1 lowercase 1 symbol and 1 number',
      );
    }
    if (mobileNo.length === 0 || mobileNo.length !== 10) {
      return showAlert('Please provide a valid mobile no');
    }
    if (selectedCity === 0) {
      return showAlert('Please select a city');
    }
    if (userType !== 2 && userType !== 3) {
      return showAlert('Please select an user type');
    }
    if (countryCode == '0') {
      return showAlert('Please select a country code');
    }

    const phone_num = countryCode + mobileNo;

    const signupBody = {
      fullName,
      username,
      email,
      password,
      phone_num,
      selectedCity,
      userType,
    };
    //console.log("SignData-> ",signupBody);

    dispatch(signupUser(signupBody, navigation));
  };

  return (
    <View style={styles.screenContainer}>
      <ProgressLoader loading={loader} />
      <Statusbar backgroundColor={'black'} />
      <ScrollView
        style={{ flexGrow: 1 }}
        ref={scrollViewRef}
        keyboardShouldPersistTaps="handled">
        <ImageBackground
          source={backgroundImage}
          style={styles.backgroundImage}>
          <Image source={logoImage} style={styles.logoImage} />
          <View style={styles.subContainer}>
            <Input
              placeholder="Full Name"
              value={fullName}
              onChangeText={fullNameInputHandeler}
              placeholderTextColor="goldenrod"
              color="goldenrod"
              inputContainerStyle={{ borderColor: Colors.sports }}
              leftIcon={
                <Icon
                  name="user-o"
                  type="font-awesome"
                  size={24}
                  color="goldenrod"
                />
              }
            />
            <Input
              placeholder="Username"
              value={username}
              onChangeText={usernameInputHandeler}
              placeholderTextColor="goldenrod"
              color="goldenrod"
              inputContainerStyle={{ borderColor: Colors.sports }}
              leftIcon={
                <Icon
                  name="user-o"
                  type="font-awesome"
                  size={24}
                  color="goldenrod"
                />
              }
            />
            <Input
              placeholder="Email"
              value={email}
              onChangeText={emailInputHandeler}
              placeholderTextColor="goldenrod"
              color="goldenrod"
              inputContainerStyle={{ borderColor: Colors.sports }}
              leftIcon={
                <Icon
                  name="envelope-o"
                  type="font-awesome"
                  size={24}
                  color="goldenrod"
                />
              }
            />

            <Input
              placeholder="Password"
              value={password}
              onChangeText={passwordInputHandeler}
              placeholderTextColor="goldenrod"
              color="goldenrod"
              secureTextEntry={isPasswordVisible}
              inputContainerStyle={{ borderColor: Colors.sports }}
              leftIcon={
                <Icon
                  name="lock"
                  type="font-awesome"
                  size={24}
                  color="goldenrod"
                />
              }
              rightIcon={
                <Icon
                  name={isPasswordVisible ? 'eye-slash' : 'eye'}
                  type="font-awesome"
                  size={normalize(22)}
                  color={Colors.whiteColor}
                  onPress={getPasswordVisiblity}
                />
              }
            />
            <View style={styles.typeContainer}>
              <Text
                style={{
                  fontSize: normalize(12),
                  color: 'goldenrod',
                }}>
                COUNTRY CODE
              </Text>
            </View>
            <View
              style={{
                borderWidth: 1,
                borderColor: Colors.sports,
                borderRadius: 4,
                backgroundColor: Colors.sports,
              }}>
              <Picker
                style={styles.picker}
                selectedValue={countryCode}
                style={{
                  width: normalize(240),
                  color: 'black',
                }}
                dropdownIconColor="black"
                onValueChange={(itemValue, itemIndex) =>
                  setCountryCode(itemValue)
                }>
                <Picker.Item key={'0'} label={'Select'} value={'0'} />
                <Picker.Item key={'91'} label={'IND'} value={'91'} />
                <Picker.Item key={'01'} label={'USA'} value={'1'} />
              </Picker>
            </View>
            <Input
              placeholder="Mobile Number"
              value={mobileNo}
              onChangeText={mobileNoInputhandler}
              placeholderTextColor="goldenrod"
              color="goldenrod"
              keyboardType="number-pad"
              inputContainerStyle={{
                borderColor: Colors.sports,
                width: normalize(240),
                paddingTop: normalize(10),
              }}
              leftIcon={
                <Icon name="call" type="zocial" size={24} color="goldenrod" />
              }
            />
            <View style={styles.typeContainer}>
              <Text
                style={{
                  fontSize: normalize(12),
                  color: 'goldenrod',
                }}>
                CITY
              </Text>
            </View>
            <View
              style={{
                borderWidth: 1,
                borderColor: Colors.sports,
                borderRadius: 4,
                backgroundColor: Colors.sports,
              }}>
              <Picker
                style={styles.picker}
                selectedValue={selectedCity}
                style={{
                  width: normalize(240),
                  color: 'black',
                }}
                dropdownIconColor="black"
                onValueChange={(itemValue, itemIndex) =>
                  setSelectedCity(itemValue)
                }>
                <Picker.Item key={0} label={'Select'} value={0} />
                {cities.map(item => (
                  <Picker.Item
                    key={item.id}
                    label={item.name}
                    value={item.id}
                  />
                ))}
              </Picker>
            </View>

            <View style={styles.typeContainer}>
              <Text
                style={{
                  fontSize: normalize(12),
                  color: 'goldenrod',
                }}>
                USER TYPE
              </Text>
              <View style={styles.RBMainContainer}>
                <CheckBox
                  title="Player"
                  checkedIcon="dot-circle-o"
                  uncheckedIcon="circle-o"
                  checkedColor="goldenrod"
                  uncheckedColor="goldenrod"
                  onPress={() => setUserType(2)}
                  checked={userType == 2}
                  textStyle={styles.radioText}
                  containerStyle={styles.radioContainer}
                />
                <CheckBox
                  title="Scorer"
                  checkedIcon="dot-circle-o"
                  uncheckedIcon="circle-o"
                  checkedColor="goldenrod"
                  uncheckedColor="goldenrod"
                  onPress={() => setUserType(3)}
                  checked={userType == 3}
                  textStyle={styles.radioText}
                  containerStyle={styles.radioContainer}
                />
              </View>
            </View>
            <Button
              title="SIGN UP"
              titleStyle={{
                color: 'black',
                fontSize: normalize(15),
              }}
              buttonStyle={{
                backgroundColor: Colors.sports,
                width: normalize(260),
              }}
              onPress={signupHandler}
            />
          </View>
        </ImageBackground>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoImage: {
    flex: 1,
    resizeMode: 'center',
    marginTop: normalize(-100),
  },
  subContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '80%',
    marginTop: Platform.OS=="android"? normalize(-50):"50%",
    paddingBottom: Platform.OS=="android"? normalize(40): "50%",
  },
  typeContainer: {
    marginBottom: normalize(10),
    width: '90%',
    alignItems: 'flex-start',
    marginVertical: normalize(8),
  },
  radioText: {
    color: '#fff',
    fontSize: normalize(12),
    fontWeight: 'normal',
  },
  radioContainer: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    marginRight: normalize(30),
  },
  RBMainContainer: {
    marginVertical: normalize(5),
    flexDirection: 'row',
  },
  picker: {
    color: 'black',
    borderWidth: 1,
    backgroundColor: Colors.sports,
  },
  phoneContainer: {
    flex: 1,
    flexDirection: 'row',
    width: '90%',
  },
});
export default Signup;
