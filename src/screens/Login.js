import React, {Fragment, useRef, useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  View,
  StyleSheet,
  ImageBackground,
  Image,
  ScrollView,
  TouchableOpacity,
  BackHandler,
  Platform
} from 'react-native';
import Colors from '../themes/Colors';
import {normalize} from '../utils/Dimention';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Input, Button, Text} from 'react-native-elements';
import Statusbar from '../components/StatusBar';
import {useNavigation} from '@react-navigation/native';
import {showAlert} from '../utils/Toast';
import {authicateUser} from '../store/login-slice';
import ProgressLoader from '../components/ProgressLoader';

const backgroundImage = require('../assets/login_bg.png');
const logoImage = require('../assets/logo_icon.png');

const Login = () => {
  const loader = useSelector(state => state.loader.loading);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const scrollViewRef = useRef();

  useEffect(() => {
    scrollViewRef.current.scrollToEnd({animated: true});
    setIsPasswordVisible(isPasswordVisible);

    const backAction = () => {
      BackHandler.exitApp();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isPasswordVisible, setIsPasswordVisible] = useState(true);

  const getPasswordVisiblity = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };

  const usernameInputHandeler = inputText => {
    setUsername(inputText);
  };
  const passwordInputHandeler = inputText => {
    setPassword(inputText);
  };
  const loginHandler = () => {
    if (username.length === 0) {
      return showAlert('Username cannot be empty');
    }
    if (password.length < 8) {
      return showAlert('Password cannot be less than 8 character');
    }
    dispatch(authicateUser(username, password, navigation));
  };

  return (
    <Fragment>
      <ProgressLoader loading={loader} />
      <View style={styles.screenContainer}>
        <Statusbar backgroundColor={'black'} />
        <ScrollView
          style={{flexGrow: 1}}
          ref={scrollViewRef}
          keyboardShouldPersistTaps="handled">
          <ImageBackground
            source={backgroundImage}
            style={styles.backgroundImage}>
            <Image source={logoImage} style={styles.logoImage} />
            <View style={styles.subContainer}>
              <Input
                placeholder="Username"
                value={username}
                onChangeText={usernameInputHandeler}
                placeholderTextColor={Colors.sports}
                color={Colors.sports}
                inputContainerStyle={{borderColor: Colors.sports}}
                leftIcon={
                  <Icon
                    name="user"
                    size={normalize(24)}
                    color={Colors.sports}
                  />
                }
              />
              <Input
                placeholder="Password"
                value={password}
                onChangeText={passwordInputHandeler}
                placeholderTextColor={Colors.sports}
                color={Colors.sports}
                secureTextEntry={isPasswordVisible}
                inputContainerStyle={{borderColor: Colors.sports}}
                leftIcon={
                  <Icon
                    name="lock"
                    size={normalize(24)}
                    color={Colors.sports}
                  />
                }
                rightIcon={
                  <Icon
                    name={isPasswordVisible ? 'eye-slash' : 'eye'}
                    size={normalize(22)}
                    color={Colors.whiteColor}
                    onPress={getPasswordVisiblity}
                  />
                }
              />
              <TouchableOpacity
                style={styles.forgotConatiner}
                // onPress={() => navigation.navigate('Forgot')}
              >
                <Text
                  style={{
                    fontSize: normalize(12),
                    color: 'white',
                  }}>
                  Forgot Password
                </Text>
              </TouchableOpacity>
              <Button
                title="LOG IN"
                titleStyle={{
                  color: 'black',
                  fontSize: normalize(15),
                }}
                buttonStyle={{
                  backgroundColor: Colors.sports,
                  width: normalize(275),
                }}
                onPress={loginHandler}
              />
              <View style={styles.textConatiner}>
                <Text
                  style={{
                    fontSize: normalize(14),
                    color: 'white',
                  }}>
                  OR
                </Text>
              </View>
              <Button
                title="LOGIN using FACEBOOK"
                titleStyle={{
                  color: 'black',
                  fontSize: normalize(15),
                }}
                buttonStyle={{
                  backgroundColor: Colors.sports,
                  width: normalize(275),
                }}
                onPress={() => navigation.navigate('Dashboard')}
              />
              <View style={styles.textConatiner}>
                <Text
                  style={{
                    fontSize: normalize(12),
                    color: Colors.sports,
                  }}>
                  Don't have an Account ?{' '}
                  <Text
                    style={{
                      fontSize: normalize(12),
                      fontWeight: 'bold',
                      color: Colors.sports,
                    }}>
                    create an Account
                  </Text>
                </Text>
              </View>
              <Button
                title="SIGN UP"
                titleStyle={{
                  color: 'black',
                  fontSize: normalize(15),
                }}
                buttonStyle={{
                  backgroundColor: 'white',
                  width: normalize(275),
                }}
                onPress={() => navigation.navigate('Signup')}
              />
            </View>
          </ImageBackground>
        </ScrollView>
      </View>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoImage: {
    flex: 1,
    resizeMode: 'center',
  },
  subContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    marginTop: Platform.OS=="android"? normalize(-50):"50%",
    paddingBottom: Platform.OS=="android"? normalize(40): "50%",
  },
  forgotConatiner: {
    marginBottom: normalize(10),
    width: '90%',
    alignItems: 'flex-end',
  },
  textConatiner: {
    marginVertical: normalize(10),
    width: '90%',
    alignItems: 'center',
  },
});

export default Login;
