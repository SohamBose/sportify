import React, { Fragment, useRef, useEffect, useState } from 'react';
import {
    View,
    StyleSheet,
    ImageBackground,
    ActivityIndicator,
    ScrollView,
    TouchableOpacity,
    Modal,
    Platform,
    PermissionsAndroid,
} from 'react-native';

import { showAlert } from '../utils/Toast';
import Colors from '../themes/Colors';
import { normalize } from '../utils/Dimention';
import { imageUrl } from '../api/apiConstants ';
import { Input, Button, Text, Avatar, Image, Icon } from 'react-native-elements';
import Statusbar from '../components/StatusBar';
import CustomHeader from '../components/CustomHeader';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import { validateEmail } from '../utils/emailValidator';
import Card from '../components/Card';
import {
    getExistingData,
    updateImage,
    updateProfileDetails,
    getOTP,
} from '../store/editProfile-slice';
import ProgressLoader from '../components/ProgressLoader';
import { Alert } from 'react-native';
import * as ImagePicker from 'react-native-image-picker';
const backgroundImage = require('../assets/edit_prof_bg.png');
const dummy_prof = require('../assets/edit_prof_dummy_img.jpg');
import AsyncStorage from '@react-native-async-storage/async-storage';

let id = '';

const UpdateDetails = () => {
    const loader = useSelector(state => state.loader.loading);
    const dispatch = useDispatch();
    const existing_data = useSelector(state => state.editprofile.userdata);
    
    const navigation = useNavigation();
    const scrollViewRef = useRef();

    const [phone, setPhone] = useState(null);
    const [password, setPassword] = useState(null);
    const [isPasswordVisible, setIsPasswordVisible] = useState(true);

    useEffect(async () => {
        //scrollViewRef.current.scrollToEnd({ animated: true });
        id = await AsyncStorage.getItem('global_ID');
        dispatch(getExistingData(id));

    }, []);


    const getPasswordVisiblity = () => {
        setIsPasswordVisible(!isPasswordVisible);
    };

    const updatePhoneNo = async () => {
        const phone_num = '' + existing_data.phone;
        if (phone != null) {
            if (phone != phone_num) {
                if (phone.length == 10) {
                    dispatch(getOTP(id, phone, navigation));
                } else {
                    showAlert("Please provide a valid contact number");
                }
            }
        }
    };

    

    return (
        <View style={styles.screenContainer}>
            <ProgressLoader loading={loader} />
            <Statusbar backgroundColor={'black'} />
            <CustomHeader navigationProps={navigation} />
            <ScrollView style={{ flexGrow: 1 }} ref={scrollViewRef}>
                <ImageBackground
                    source={backgroundImage}
                    style={styles.backgroundImage}>
                    <Card
                        style={
                            ([styles.backgroundImage],
                            {
                                backgroundColor: 'rgba(52, 52, 52, 0.8)',
                                top: "15%",
                                width: '90%',
                                marginBottom: normalize(50),
                                borderRadius: normalize(5),
                            })
                        }>
                        <Text
                            style={{
                                fontSize: normalize(16),
                                color: '#fff',
                                fontWeight: 'normal',
                                top: normalize(20),
                            }}>
                            Update Details
                        </Text>

                        <View style={styles.subContainer}>

                            <Input
                                defaultValue={!existing_data.phone ? "" : '' + existing_data.phone}
                                placeholderTextColor={Colors.sports}
                                onChangeText={inputtext => setPhone(inputtext)}
                                color={Colors.sports}
                                keyboardType="number-pad"
                                inputContainerStyle={styles.inputContainer}
                                inputStyle={styles.inputstyleContainer}
                                leftIcon={
                                    <Icon
                                        name="call"
                                        type="zocial"
                                        size={normalize(18)}
                                        color={Colors.sports}
                                    />
                                }

                            />

                            <Input
                                color={Colors.sports}
                                placeholderTextColor={Colors.sports}
                                disabled={true}
                                value="Old Password"
                                onChangeText={inputtext => setPassword(inputtext)}
                                secureTextEntry={isPasswordVisible}
                                inputContainerStyle={styles.inputContainer}
                                inputStyle={styles.inputstyleContainer}
                                leftIcon={
                                    <Icon
                                        name="lock"
                                        type="font-awesome"
                                        size={normalize(18)}
                                        color={Colors.sports}
                                    />
                                }
                                rightIcon={
                                    <Icon
                                      name={isPasswordVisible ? 'eye-slash' : 'eye'}
                                      type="font-awesome"
                                      size={normalize(18)}
                                      color={Colors.whiteColor}
                                      iconStyle={{ right: normalize(1) }}
                                      onPress={getPasswordVisiblity}
                                    />
                                  }
                            />

                            <Button
                                title="UPDATE"
                                titleStyle={{
                                    color: 'black',
                                    fontSize: normalize(15),
                                }}
                                buttonStyle={{
                                    backgroundColor: Colors.sports,
                                    width: normalize(215),
                                }}
                                containerStyle={{ top: normalize(25) }}
                                onPress={() => {
                                    updatePhoneNo();
                                  }}
                            />
                        </View>
                    </Card>
                </ImageBackground>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    screenContainer: {
        flex: 1,
    },
    backgroundImage: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: "50%"
    },

    subContainer: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        width: '80%',
        paddingBottom: normalize(130),
        top: "15%",
    },

    inputContainer: {
        borderBottomWidth: 0,
        height: normalize(35),
        borderRadius: 2,
        backgroundColor: 'black',
        color: 'white',
        width: '100%',
        paddingLeft: 20,
        marginTop: 10,
        marginBottom: 10,
    },
    inputstyleContainer: {
        fontSize: normalize(15),
    },
    profileImg: {
        width: normalize(65),
        height: normalize(65),
        borderRadius: normalize(200 / 2),
        borderColor: Colors.sports,
        borderWidth: 1,
    },
    addImageStyle: {
        position: 'absolute',
        alignSelf: 'flex-end',
        right: normalize(103),
        bottom: '68%',
    },

    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040',

    },
    activityIndicatorWrapper: {
        backgroundColor: '#fff',
        height: normalize(170),
        width: normalize(160),
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'column',
        borderRadius: normalize(2),
        marginTop: normalize(8),
    },
    pickerOptionStyle: {
        flexDirection: "row",
        display: "flex",
        alignItems: 'center',
        justifyContent: 'space-around',
        right: normalize(30)
    },
    pickerTextStyle: {
        fontWeight: '700',
        fontSize: normalize(10),
        left: normalize(10),
    }

});

export default UpdateDetails;