import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  StyleSheet,
  ImageBackground,
  Image,
  ScrollView,
  FlatList,
  Text,
  LogBox,
  BackHandler,
} from 'react-native';

import Colors from '../themes/Colors';
import {normalize} from '../utils/Dimention';
import {
  Input,
  ButtonGroup,
  Button,
  Icon,
  CheckBox,
  Card,
} from 'react-native-elements';
import Statusbar from '../components/StatusBar';

import {useNavigation, useFocusEffect} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';
import {getSportsList, updateSportsList} from '../store/sportsinterest-slice';
import ProgressLoader from '../components/ProgressLoader';
import {showAlert} from '../utils/Toast';
import {TouchableOpacity} from 'react-native';

const backgroundImage = require('../assets/signup_bg.png');
const logoImage = require('../assets/logo_icon.png');
const tick_icon = require('../assets/check_icon.png');

const SportInterest = () => {
  const loader = useSelector(state => state.loader.loading);
  const isLoggedin = useSelector(state => state.login.isLoggedin);

  const dispatch = useDispatch();
  const sports_data_arr = useSelector(state => state.sportsinterest.sports);
  const scrollViewRef = useRef();
  const navigation = useNavigation();

  useEffect(() => {
    scrollViewRef.current.scrollToEnd({animated: true});
  });

  useFocusEffect(
    React.useCallback(() => {
      //Screen focused
      scrollViewRef.current.scrollToEnd({animated: true});
      dispatch(getSportsList());

      if (!isLoggedin) {
        const backAction = () => {
          BackHandler.exitApp();
          return true;
        };

        const backHandler = BackHandler.addEventListener(
          'hardwareBackPress',
          backAction,
        );

        return () => backHandler.remove();
      }
    }, []),
  );

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);

  const RenderFlatList = () => (
    <FlatList
      style={{width: '100%', height: '100%'}}
      data={sports_data_arr}
      keyExtractor={(item, index) => index}
      extraData={sports_data_arr}
      renderItem={({item, index}) => (
        <>
          {
            <Card containerStyle={styles.cardMainBlock}>
              <TouchableOpacity onPress={onGridItemClick.bind(this, item)}>
                <ImageBackground
                  source={{uri: item.image_path + item.image}}
                  style={styles.cardBg}>
                  {item.selected_stat ? (
                    <Image source={tick_icon} style={styles.tickStyle} />
                  ) : null}

                  <Text style={styles.sportTitleStyle}>
                    {item.name.toUpperCase()}
                  </Text>
                </ImageBackground>
              </TouchableOpacity>
            </Card>
          }
        </>
      )}
      numColumns={2}
    />
  );

  const onGridItemClick = item => {
    try {
      let objIndex = sports_data_arr.findIndex(obj => obj.id == item.id);
      dispatch(updateSportsList(objIndex));
    } catch (e) {
      console.log(e);
    }
  };

  const nextHandler = () => {
    const selected = sports_data_arr.filter(item => {
      return item.selected_stat === true;
    });

    if (selected.length === 0) {
      return showAlert('Please select atleast 1 sport');
    }
    navigation.navigate('Dashboard');
  };

  const skipHandler = () => {
    try {
      navigation.navigate('Dashboard');
    } catch (error) {
      console.log('FromSports--> ', error);
    }
  };

  return (
    <View style={styles.screenContainer}>
      <ProgressLoader loading={loader} />
      <Statusbar backgroundColor={'black'} />
      <ScrollView style={{flexGrow: 1}} ref={scrollViewRef}>
        <ImageBackground
          source={backgroundImage}
          style={styles.backgroundImage}>
          <Image source={logoImage} style={styles.logoImage} />
          <View style={styles.subContainer}>
            <Text
              style={{
                fontSize: normalize(18),
                color: '#fff',
                fontWeight: 'normal',
                bottom: normalize(35),
              }}>
              Sports Interest
            </Text>

            <View style={styles.cardMainContainer}>
              <RenderFlatList />
            </View>

            <View style={styles.buttonConatiner}>
              <Button
                title="SKIP"
                titleStyle={{
                  color: 'black',
                  fontSize: normalize(12),
                }}
                buttonStyle={{
                  backgroundColor: Colors.sports,
                  width: normalize(80),
                }}
                onPress={() => skipHandler()}
              />
              <Button
                title="NEXT"
                titleStyle={{
                  color: 'black',
                  fontSize: normalize(12),
                }}
                buttonStyle={{
                  backgroundColor: Colors.sports,
                  width: normalize(80),
                }}
                onPress={nextHandler.bind(this)}
              />
            </View>
          </View>
        </ImageBackground>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoImage: {
    flex: 1,
    resizeMode: 'center',
  },
  subContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  buttonConatiner: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
    paddingBottom: normalize(20),
    bottom: normalize(30),
  },

  cardMainContainer: {
    width: '79%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  cardBg: {
    resizeMode: 'cover',
    width: normalize(118),
    height: normalize(86),
  },

  cardMainBlock: {
    justifyContent: 'center',
    flex: 2,
    alignItems: 'center',
    height: normalize(80),
    width: '100%',
    marginHorizontal: normalize(5),
    marginVertical: normalize(12),
    marginBottom: normalize(1),
    borderRadius: 5,
    borderWidth: 0,
    backgroundColor: 'transparent',
  },
  tickStyle: {
    flex: 1,
    resizeMode: 'contain',
    width: normalize(35),
    height: normalize(35),
    alignSelf: 'center',
    bottom: normalize(15),
  },
  sportTitleStyle: {
    fontSize: normalize(10),
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
    alignItems: 'flex-end',
    textAlignVertical: 'bottom',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    paddingVertical: normalize(6),
    justifyContent: 'space-around',
  },
});
export default SportInterest;
