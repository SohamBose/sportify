import React from 'react';
import {View, StyleSheet} from 'react-native';

import Colors from '../themes/Colors';
import {normalize} from '../utils/Dimention';

const Card = props => {
  return (
    <View style={{...styles.conatiner, ...props.style}}>{props.children}</View>
  );
};

const styles = StyleSheet.create({
  conatiner: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'grey',
    borderRadius: normalize(30),
    opacity: 0.7,
    marginVertical: normalize(20),
  },
});

export default Card;
