import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, SafeAreaView, Alert } from 'react-native';
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import Colors from '../themes/Colors';
import { normalize } from '../utils/Dimention';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Text, Icon } from 'react-native-elements';
import { setLogOut } from '../store/login-slice';
import { useSelector, useDispatch } from 'react-redux';

const CustomSidebar = props => {
  const { state, descriptors, navigation } = props;
  const [menu_state, setMenuState] = useState('Home            ');
  const dispatch = useDispatch();

  useEffect(() => {
    setMenuState(menu_state);
  }, []);

  let lastGroupName = '';
  let newGroup = true;

  const navigateTo = async (menuName, nav_name) => {
    if (menuName == "Log Out") {
      Alert.alert(
        "Alert !",
        "Do you want to log out ?",
        [
          {
            text: "No",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          {
            text: "Yes", onPress: async () => {
              dispatch(setLogOut());
              await AsyncStorage.removeItem("auth_token");
              navigation.navigate(nav_name);
            }
          }
        ]
      );
    } else {
      setMenuState(menuName);
    }
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <DrawerContentScrollView {...props}>
        {state.routes.map(route => {
          const {
            drawerLabel,
            activeTintColor,
            groupName,
            icon_name,
            icon_type,
          } = descriptors[route.key].options;
          if (lastGroupName !== groupName) {
            newGroup = true;
            lastGroupName = groupName;
          } else newGroup = false;
          return (
            <View key={route.key}>
              {newGroup ? (
                <TouchableOpacity style={styles.sectionView} onPress={navigateTo.bind(this, groupName, route.name)}>
                  <Icon
                    name={icon_name}
                    type={icon_type}
                    size={20}
                    color={Colors.warm_grey}
                    style={{ marginLeft: normalize(20) }}
                  />

                  <Text
                    key={groupName}
                    style={{
                      left: 10,
                      fontWeight: '500',
                      color:
                        groupName == 'Log Out'
                          ? Colors.warm_grey
                          : Colors.sports,
                      fontSize: normalize(12),
                    }}>
                    {groupName}
                  </Text>
                  {groupName != 'Log Out' ? (
                    <Icon
                      name={menu_state == groupName ? "chevron-up" : "chevron-down"}
                      type="font-awesome"
                      size={15}
                      color={Colors.sports}
                      style={{ marginLeft: normalize(25) }}
                    />

                  ) : null}

                </TouchableOpacity>
              ) : null}

              {menu_state == groupName ? (
                <DrawerItem
                  label={({ color }) => (
                    <Text
                      style={{ color, marginLeft: normalize(40), color: '#fff' }}>
                      {drawerLabel}
                    </Text>
                  )}
                  focused={
                    state.routes.findIndex(e => e.name === route.name) ===
                    state.index
                  }
                  activeTintColor={activeTintColor}
                  onPress={() => navigation.navigate(route.name)}
                />
              ) : null}
            </View>
          );
        })}
      </DrawerContentScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },

  sectionView: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 12,
    width: '100%',
  },

  mainContainer: {
    width: '100%',
    height: '10%',
    backgroundColor: 'black',
  },
});

export default CustomSidebar;
