import React from "react";
import { View, StatusBar, Platform } from "react-native";
import propTypes from "prop-types";
import Colors from '../themes/Colors';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {normalize} from '../utils/Dimention'
const MyStatusBar = ({ backgroundColor, barStyle, height, ...props }) =>
(
    <View
        style={[
            { height: getStatusBarHeight() },
            { backgroundColor }
        ]}
    >
        <StatusBar
            translucent
            backgroundColor={backgroundColor}
            {...props}
            barStyle={barStyle}
            hidden={false}
        />
    </View>
);

export default MyStatusBar;
MyStatusBar.propTypes = {
    backgroundColor: propTypes.string,
    barStyle: propTypes.string,
    height: propTypes.number,
};

MyStatusBar.defaultProps = {
    backgroundColor: Colors.StatusBar,
    barStyle: "light-content",
    height: normalize(20),
};