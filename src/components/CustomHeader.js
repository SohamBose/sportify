import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  StyleSheet,
  ImageBackground,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

import Colors from '../themes/Colors';
const logo = require('../assets/logo.png');
const menu_icon = require('../assets/menu_icon.png');

import {normalize} from '../utils/Dimention';
import {Icon, Text} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';

const CustomHeader = props => {
  return (
    <View style={styles.mainContainer}>
      <View
        style={{
          flexDirection: 'row',
        }}>
        <TouchableOpacity style={{width:"18%"}} onPress={() => props.navigationProps.toggleDrawer()}>
          <View style={styles.iconStyle}>
            <Image source={menu_icon} style={styles.logoStyle} />
          </View>
        </TouchableOpacity>
        <View
          style={{
            top: normalize(12),
            left: normalize(80),
          }}>
          <Image source={logo} style={styles.logoStyle} />
        </View>

        <View
          style={{
            top: normalize(12),
            left: normalize(85),
          }}>
          <Text style={styles.textStyle}>Sportifi</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    width: '100%',
    height: '10%',
    backgroundColor: 'black',
  },
  iconStyle: {
    alignContent: 'flex-start',
    alignItems: 'flex-start',
    left: normalize(8),
    top: normalize(10),
  },
  logoStyle: {
    width: normalize(25),
    height: normalize(25),
    resizeMode: 'center',
  },
  textStyle: {
    fontSize: normalize(16),
    color: Colors.sports,
    fontWeight: 'normal',
    textAlign: 'center',
    alignContent: 'center',
    alignSelf: 'center',
  },
});

export default CustomHeader;
