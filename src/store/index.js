import {configureStore} from '@reduxjs/toolkit';
import signUpSlice from './singup-slice';
import loginSlice from './login-slice';
import loaderSlice from './loader-slice';
import sportsInterestSlice from './sportsinterest-slice';
import leagueTypesSlice from './leagueTypes-slice';
import editProfileSlice from './editProfile-slice';

const store = configureStore({
  reducer: {
    signup: signUpSlice.reducer,
    login: loginSlice.reducer,
    loader: loaderSlice.reducer,
    sportsinterest: sportsInterestSlice.reducer,
    leagueTypesSlice: leagueTypesSlice.reducer,
    editprofile: editProfileSlice.reducer,
  },
});

export default store;
