import {createSlice} from '@reduxjs/toolkit';
import {getLeagueTypesApi} from '../api/LeagueTypesScreenApi';
import {loaderActions} from './loader-slice';

const leagueTypesSlice = createSlice({
  name: 'leagueTypesSlice',
  initialState: {types: []},
  reducers: {
    addLeagueTypes(state, action) {
      state.types = action.payload;
    },
    updateLeagueTypes(state, action) {
      let pos = action.payload;
      state.types[pos].selected_stat = !state.types[pos].selected_stat;
    },
  },
});

export const getLeagueTypes = () => {
  return async dispatch => {
    try {
      dispatch(loaderActions.toggele());
      const data = await getLeagueTypesApi();
      let data_Arr = data.data.map(arr => ({...arr, selected_stat: false}));
      dispatch(leagueTypesSlice.actions.addLeagueTypes(data_Arr));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(loaderActions.toggele());
    }
  };
};

export const updateLeagueTypesList = item_index => {
  return dispatch => {
    try {
      dispatch(leagueTypesSlice.actions.updateLeagueTypes(item_index));
    } catch (error) {
      console.log(error);
    }
  };
};

export default leagueTypesSlice;
