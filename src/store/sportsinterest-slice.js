import { createSlice } from '@reduxjs/toolkit';
import { getAllSportsApi } from '../api/sportsInterestApi';
import { loaderActions } from './loader-slice';

const sportsInterestSlice = createSlice({
  name: 'sportsinterest',
  initialState: { sports: [] },
  reducers: {
    showSports(state, action) {
      state.sports = action.payload;
    },
    updateSports(state, action) {
      let pos = action.payload;
      //let modified_arr = [...state.sports];
      state.sports[pos].selected_stat = !(state.sports[pos].selected_stat);

    },
  },
});

export const getSportsList = () => {
  return async dispatch => {
    try {
      dispatch(loaderActions.toggele());
      const data = await getAllSportsApi();
      let data_Arr = data.data.map(arr => ({ ...arr, selected_stat: false }));
      dispatch(sportsInterestSlice.actions.showSports(data_Arr));
      dispatch(loaderActions.toggele());
    } catch (error) {
      console.log(error);
      dispatch(loaderActions.toggele());
    }
  };
};

export const updateSportsList = (item_index) => {
  return dispatch => {
    try {
      dispatch(sportsInterestSlice.actions.updateSports(item_index));
    } catch (error) {
      console.log(error);
    }
  };
};

export default sportsInterestSlice;
