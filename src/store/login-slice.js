import {createSlice} from '@reduxjs/toolkit';
import {postLogin} from '../api/loginScreenApi';
import {showAlert} from '../utils/Toast';
import {loaderActions} from './loader-slice';
import AsyncStorage from '@react-native-async-storage/async-storage';

const loginSlice = createSlice({
  name: 'login',
  initialState: {isLoggedin: false, splashScreen: false},
  reducers: {
    login(state) {
      state.isLoggedin = true;
    },
    logout(state) {
      state.isLoggedin = false;
    },
    splashScreenRemove(state) {
      state.splashScreen = true;
    },
  },
});

export const authicateUser = (username, password, navigation) => {
  return async dispatch => {
    let msg = '';
    try {
      dispatch(loaderActions.toggele());
      const data = await postLogin(username, password);
      if (!data.status) {
        msg = data.message;
        return;
      }
      if (data.status && !data.data.phone_verified) {
        msg = 'Please verify phone';
        await AsyncStorage.setItem('auth_token', '' + data.auth_token);
        return navigation.navigate('OtpScreen', {
          user_id: data.data.id,
        });
      }
      console.log('LoginData::--> ', data);
      msg = 'Login success';
      await AsyncStorage.setItem('global_ID', '' + data.data.id);
      await AsyncStorage.setItem('auth_token', '' + data.auth_token);
      navigation.navigate('Dashboard');
      dispatch(loginSlice.actions.login());
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(loaderActions.toggele());
      setTimeout(() => {
        showAlert(msg);
      }, 500);
    }
  };
};


export const setLogOut = () => {
  return dispatch => {
    try {
      dispatch(loginSlice.actions.logout());
    } catch (error) {
      console.log(error);
    }
  };
};

export const LoginAction = loginSlice.actions;
export default loginSlice;
