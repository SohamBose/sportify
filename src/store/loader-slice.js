import {createSlice} from '@reduxjs/toolkit';

const loaderSlice = createSlice({
  name: 'loader',
  initialState: {loading: false},
  reducers: {
    toggele(state) {
      state.loading = !state.loading;
    },
  },
});

export const loaderActions = loaderSlice.actions;
export default loaderSlice;
