import {createSlice} from '@reduxjs/toolkit';
import {postSignupUser, getCityApi} from '../api/signUpScreenApi';
import {postPhoneOtpValidate, postResendOtpApi} from '../api/OtpScreenApi';
import {loaderActions} from './loader-slice';
import {showAlert} from '../utils/Toast';
import AsyncStorage from '@react-native-async-storage/async-storage';

const signUpSlice = createSlice({
  name: 'signup',
  initialState: {city: []},
  reducers: {
    addCity(state, action) {
      state.city = action.payload;
    },
  },
});

export const getCity = () => {
  return async dispatch => {
    try {
      dispatch(loaderActions.toggele());
      const data = await getCityApi();
      dispatch(signUpSlice.actions.addCity(data.cities));
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(loaderActions.toggele());
    }
  };
};

export const signupUser = (signup_data, navigation) => {
  return async dispatch => {
    let msg = '';
    try {
      dispatch(loaderActions.toggele());
      const data = await postSignupUser(signup_data);
      if (!data.status) {
        msg = data.message;
        return;
      }
      msg = 'User created. Please verify phone';
      await AsyncStorage.setItem('global_ID', '' + data.data.id);
      await AsyncStorage.setItem('auth_token', '' + data.auth_token);
      navigation.navigate('OtpScreen', {
        user_id: data.data.id,
        phone_no: data.data.phone,
      });
    } catch (error) {
      console.log(console.log(error));
    } finally {
      dispatch(loaderActions.toggele());
      setTimeout(() => {
        showAlert(msg);
      }, 500);
    }
  };
};

export const phoneOtpValidate = (user_id, otp, navigation) => {
  return async dispatch => {
    let msg = '';
    try {
      dispatch(loaderActions.toggele());
      const data = await postPhoneOtpValidate(user_id, otp);
      if (!data.status) {
        msg = data.message;
        return;
      }
      msg = data.message;
      navigation.navigate('LeagueType');
    } catch (error) {
      console.log(console.log(error));
    } finally {
      dispatch(loaderActions.toggele());
      setTimeout(() => {
        showAlert(msg);
      }, 500);
    }
  };
};

export const resendOtp = (user_id, phone_no) => {
  return async dispatch => {
    let msg = '';
    try {
      dispatch(loaderActions.toggele());
      const data = await postResendOtpApi(user_id, phone_no);
      if (!data.status) {
        msg = data.message;
        return;
      }
      msg = data.message;
    } catch (error) {
      console.log(console.log(error));
    } finally {
      dispatch(loaderActions.toggele());
      setTimeout(() => {
        showAlert(msg);
      }, 500);
    }
  };
};

export default signUpSlice;
