import { createSlice } from '@reduxjs/toolkit';
import { fetchUserData, updateProfileImage, updateProfile, getPhoneOTP, updatePhoneNumber } from '../api/EditProfileApi';
import { loaderActions } from './loader-slice';
import { showAlert } from '../utils/Toast';
const editProfileSlice = createSlice({
    name: 'editprofile',
    initialState: { userdata: {} },
    reducers: {
        displayUserData(state, action) {
            state.userdata = action.payload;
        },
    },
});


export const getExistingData = (userID) => {
    return async dispatch => {
        try {
            dispatch(loaderActions.toggele());
            const data = await fetchUserData(userID);

            dispatch(editProfileSlice.actions.displayUserData(data.data));
            dispatch(loaderActions.toggele());
        } catch (error) {
            console.log(error);
            dispatch(loaderActions.toggele());
        }
    };
};

export const updateImage = (userID, image_uri, img_type, image_name) => {
    return async dispatch => {
        let msg = "";
        try {
            dispatch(loaderActions.toggele());
            //const data = await updateProfileImage(userID,image_uri,img_type,image_name);
            updateProfileImage(userID, image_uri, img_type, image_name)
                .then(res => msg = res.message);
        } catch (error) {
            console.log("EditError::", error);
        } finally {
            dispatch(loaderActions.toggele());
            setTimeout(() => {
                showAlert(msg);
            }, 500);
        }
    };
};


export const updateProfileDetails = (userData, navigation) => {
    return async dispatch => {
        let msg = "";
        try {
            dispatch(loaderActions.toggele());
            const data = await updateProfile(userData);
            if (!data.status) {
                msg = data.message;
                return;
            }

            msg = data.message;
            navigation.navigate('Dashboard');
        } catch (error) {
            console.log("EditError::", error);
        } finally {
            dispatch(loaderActions.toggele());
            setTimeout(() => {
                showAlert(msg);
            }, 800);
        }
    };
};



export const getOTP = (userID, phone, navigation) => {
    return async dispatch => {
        let msg = "";
        try {
            dispatch(loaderActions.toggele());
            const data = await getPhoneOTP(userID, phone);
            if (!data.status) {
                msg = data.message;
                return;
            }

            msg = data.message;
            navigation.navigate('OtpScreen', {
                user_id: userID,
                phone_no: phone,
                isEdit: true,
            });
        } catch (error) {
            console.log("EditError::", error);
        } finally {
            dispatch(loaderActions.toggele());
            setTimeout(() => {
                showAlert(msg);
            }, 500);
        }
    };
};


export const updatePhone = (userID, phone, OTP, navigation) => {
    return async dispatch => {
        let msg = "";
        try {
            dispatch(loaderActions.toggele());
            const data = await updatePhoneNumber(userID, phone,OTP);
            if (!data.status) {
                msg = data.message;
                return;
            }

            msg = data.message;
            navigation.navigate('Dashboard');

        } catch (error) {
            console.log("EditError::", error);
        } finally {
            dispatch(loaderActions.toggele());
            setTimeout(() => {
                showAlert(msg);
            }, 500);
        }
    };
};


export default editProfileSlice;