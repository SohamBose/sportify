import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector} from 'react-redux';

import Login from '../screens/Login';
import Signup from '../screens/Signup';
import LeagueType from '../screens/LeagueType';
import SportInterest from '../screens/SportInterest';
import OtpScreen from '../screens/OtpScreen';
import SplashScreen from '../screens/SplashScreen';
import Forgot from '../screens/Forgot';

import DrawerNav from './DrawerNav';

const Stack = createStackNavigator();

const MainNavigator = () => {
  const isLoggedin = useSelector(state => state.login.isLoggedin);
  const isSplashScreen = useSelector(state => state.login.splashScreen);
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={!isSplashScreen ? 'SplashScreen' : 'Login'} 
        screenOptions={{
          headerShown: false,
        }}>
        {!isLoggedin && (
          <Stack.Screen
            name="Login"
            component={Login}
            options={{headerShown: false}}
          />
        )}
        <Stack.Screen
          name="Dashboard"
          component={DrawerNav}
          options={{headerShown: false}}
        />

        <Stack.Screen
          name="Forgot"
          component={Forgot}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Signup"
          component={Signup}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="OtpScreen"
          component={OtpScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="LeagueType"
          component={LeagueType}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SportInterest"
          component={SportInterest}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="EditProfile"
          component={DrawerNav}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="UpdatePhonePass"
          component={DrawerNav}
          options={{headerShown: false}}
        />
        {!isSplashScreen && (
          <Stack.Screen
            name="SplashScreen"
            component={SplashScreen}
            options={{headerShown: false}}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MainNavigator;
