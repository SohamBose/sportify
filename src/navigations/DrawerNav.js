import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Colors from '../themes/Colors';
import CustomSidebar from '../components/CustomSidebar';

import Dashboard from '../screens/Dashboard';
import EditProfile from '../screens/EditProfile';
import UpdateDetails from '../screens/UpdatePhonePass';
import Login from '../screens/Login';
import LeagueType from '../screens/LeagueType';
import SportInterest from '../screens/SportInterest';

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      initialRouteName="EditProfile"
      drawerStyle={{backgroundColor: Colors.blackColor}}
      drawerContent={props => <CustomSidebar {...props} />}>
      <Drawer.Screen
        name="Dashboard"
        options={{
          drawerLabel: 'Home',
          groupName: 'Home                     ',
          activeTintColor: Colors.sports,
          icon_name: 'home',
          icon_type: 'font-awesome',
        }}
        component={Dashboard}
      />
      <Drawer.Screen
        name="Profile"
        options={{
          drawerLabel: 'Edit Profile',
          groupName: 'My Profile              ',
          activeTintColor: Colors.sports,
          icon_name: 'user-o',
          icon_type: 'font-awesome',
        }}
        component={EditProfile}
      />
      <Drawer.Screen
        name="Phone"
        options={{
          drawerLabel: 'Update Details',
          groupName: 'My Profile              ',
          activeTintColor: Colors.sports,
          icon_name: 'user-o',
          icon_type: 'font-awesome',
        }}
        component={UpdateDetails}
      />

      <Drawer.Screen
        name="Team"
        options={{
          drawerLabel: 'My Team',
          groupName: 'My Profile              ',
          activeTintColor: Colors.sports,
          icon_name: 'user-o',
          icon_type: 'font-awesome',
        }}
        component={EditProfile}
      />

      <Drawer.Screen
        name="Sports Interest"
        options={{
          drawerLabel: 'Sports Interest',
          groupName: 'Sports Interest    ',
          activeTintColor: '#FF6F00',
          icon_name: 'checksquare',
          icon_type: 'ant-design',
        }}
        component={SportInterest}
      />
     
      <Drawer.Screen
        name="League Type"
        options={{
          drawerLabel: 'League Type',
          groupName: 'League Type        ',
          activeTintColor: Colors.sports,
          icon_name: 'yin-yang',
          icon_type: 'font-awesome-5',
        }}
        component={LeagueType}
      />
      
      <Drawer.Screen
        name="Logout"
        options={{
          groupName: 'Log Out',
          activeTintColor: Colors.sports,
          icon_name: 'logout',
          icon_type: 'ant-design',
        }}
        component={Login}
      />
    </Drawer.Navigator>
  );
};
export default DrawerNavigator;
